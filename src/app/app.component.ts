import {Component, Output, EventEmitter} from '@angular/core';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';

import {TodoDataService} from './todo-data.service';
import {Todo} from './todo';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [TodoDataService]
})
export class AppComponent {

  constructor(todoDataService: TodoDataService) {
    this.todoDataService = todoDataService;
  }

  editTodoId: Number;
  newTodo: Todo = new Todo();

  private todoDataService: TodoDataService;

  get todos() {
    return this.todoDataService.getAllTodos();
  }

  onStartEdit(todoId: number) {
    this.editTodoId = todoId;
  }

  onEndEdit(todo: Todo, value: string) {
    this.todoDataService.updateTodo(todo.id, value);
    this.editTodoId = null;
  }

  closeTodo(todo: Todo) {
    this.todoDataService.deleteTodo(todo.id);
  }

  addTodo() {
    this.todoDataService.addTodo(this.newTodo);
    this.newTodo = new Todo();
  }

  removeTodo(todo: Todo) {
    this.todoDataService.deleteTodo(todo.id);
  }

  toggleTodoComplete(todo) {
    this.todoDataService.toggleTodoComplete(todo);
  }

  drop(event: CdkDragDrop<Todo[]>) {
    moveItemInArray(this.todoDataService.todos, event.previousIndex, event.currentIndex);
  }

  activeTodos(): Todo[] {
    return this.todoDataService.getActiveTodos();
  }

  completeTodos(): Todo[] {
    return this.todoDataService.getCompleteTodos();
  }
}
