import {Injectable} from '@angular/core';
import {Todo} from './todo';

@Injectable({
  providedIn: 'root'
})
export class TodoDataService {

  lastId = 0;

  todos: Todo[] = [];

  constructor() {
  }

  addTodo(todo: Todo): TodoDataService {
    if (!todo.id) {
      todo.id = ++this.lastId;
    }

    this.todos.push(todo);

    return this;
  }

  deleteTodo(id: number): TodoDataService {
    this.todos = this.todos
      .filter(todo => todo.id !== id);

    return this;
  }

  updateTodo(id: number, title: string): TodoDataService {
    const todo = this.getTodo(id);

    if (!todo) {
      return null;
    }

    todo.title = title;
    return this;
  }

  getTodo(id: number): Todo {
    return this.todos
      .filter(todo => todo.id === id)
      .pop();
  }

  getAllTodos(): Todo[] {
    return this.todos;
  }

  getActiveTodos(): Todo[] {
    return this.todos
      .filter(todo => todo.complete === false);
  }

  getCompleteTodos(): Todo[] {
    return this.todos
      .filter(todo => todo.complete === true);
  }

  toggleTodoComplete(todo: Todo) {
    todo.complete = !todo.complete;
  }
}
